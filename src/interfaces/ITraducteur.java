package interfaces;

public interface ITraducteur {

	public String traduit(String mot);
}
