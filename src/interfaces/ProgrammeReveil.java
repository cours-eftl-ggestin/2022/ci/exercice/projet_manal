package interfaces;

public class ProgrammeReveil {

	public static void main(String[] args) {
		RadioReveil radioReveil = new RadioReveil(true);
		radioReveil.start();
		
		RadioReveil radioReveil2 = new RadioReveil(false);
		radioReveil2.start();
	}

}
