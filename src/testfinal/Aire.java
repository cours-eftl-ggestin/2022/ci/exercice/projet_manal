package testfinal;

public class Aire {

	public static final double pi = 3.141592653;
	public final double e = 2.718281828;
	
	public void modification() {
		System.out.println("Valeur de pi : " + pi);
		System.out.println("Valeur de e : " + e);
		Aire.pi = 3.145;
		this.e = 2.71;
	}
}
