package heritage;

class Compte {
	
	private Double solde;
	private String proprietaire;
	
	public Compte(Double solde, String proprietaire) {
		super();
		this.solde = solde;
		this.proprietaire = proprietaire;
	}
	
	protected String getElements() {
		return "Propriétaire = " + this.proprietaire + ", solde = " + this.solde;
	}

}

class CompteBancaire extends Compte {
	private String nomBanque;
	
	public CompteBancaire(Double solde, String proprietaire, String nomBanque) {
		super(solde, proprietaire);
		this.nomBanque = nomBanque;
	}
	
	@Override
	public String getElements() {
		return super.getElements() + ", nom de la banque = " + this.nomBanque;
	}
}
