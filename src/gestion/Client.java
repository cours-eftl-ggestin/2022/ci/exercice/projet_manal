package gestion;

public class Client {
	
	private String nomClient;
	private Integer statut;
	private Adresse adresse;
	
	public Client() {
		super();
		System.out.println("Ex�cution du constructeur par d�faut");
	}
	
	public Client(String nomClient, Integer statut) {
		this.nomClient = nomClient;
		this.statut = statut;
	}
	
	public Client(String nomClient, Integer statut, String adresse) {
		this.nomClient = nomClient;
		this.statut = statut;
		this.adresse = new Adresse(adresse);
	}
	
	public Boolean creerClient(String nomClient, Integer statut, String adresse) {
		this.nomClient = nomClient;
		this.statut = statut;
		this.adresse = new Adresse();
		this.adresse.setAdresse(adresse);
		return true;
	}
	
	public Boolean creerClient(String nomClient, Integer statut, Adresse adresse) {
		this.nomClient = nomClient;
		this.statut = statut;
		this.adresse = adresse;
		return true;
	}
	
	public String getAdresse() {
		if (this.adresse != null) {
			return this.adresse.getAdresse();
		} else {
			return "Ce client n'a pas encore d'adresse";
		}
	}
	
	public Boolean changerStatut(Integer statut) {
		if (this.statut == 0) {
			return false;
		}
		this.statut = statut;
		return true;
	}
	
	@Override
	public String toString() {
		return "Voici mon client : " + super.toString() + " ; " + this.nomClient + ", " + this.statut + ", " + this.getAdresse();
	}

}

class Adresse {
	private String adresse;
	
	public Adresse() {
		
	}
	
	public Adresse(String adresse) {
		this.adresse = adresse;
	}
	
	public String getAdresse() {
		return adresse;
	}
	
	public void setAdresse(String adresse) {
		if (null != adresse && !"".equals(adresse)) {
			this.adresse = adresse;
		}
	}
}
