package gestion;

public class Gestion {
	public static void main(String[] args) {
		Client monClient = new Client();
		monClient.creerClient("Marc Gilbert", 1, "Rue des tests");
		System.out.println(monClient.getAdresse());
		
		Client deuxiemeClient = new Client("Toto", 2);
		System.out.println(deuxiemeClient.getAdresse());
		
		Client troisiemeClient = new Client("Titi", 3, "Autre rue");
		System.out.println(troisiemeClient.getAdresse());
		
		System.out.println(troisiemeClient.toString());
	}

}
