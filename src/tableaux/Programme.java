package tableaux;

public class Programme {

	public static void main(String[] args) {
		Tableau tableau = new Tableau();
		
		tableau.chargement();
		
		Tableaux tableaux = new Tableaux();
		tableaux.chargement();
		tableaux.afficheTableau();
		
		//tableau.afficheTableau();
		
		//tableau.accesTableau();
		
		TableauMultiDimension tabMulti = new TableauMultiDimension();
		tabMulti.listeTableau();
		
		TableauPassage tabPassage = new TableauPassage();
		tabPassage.envoiTableau();
		
		String uneChaine = "une chaine de caractères";
		String[] sousChaines = uneChaine.split(" ");
		for (String valeur : sousChaines) {
			System.out.println(valeur);
		}
		System.out.println(sousChaines.length);
	}
}
