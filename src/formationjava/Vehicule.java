package formationjava;

public abstract class Vehicule {
	protected Boolean type;
	
	public Vehicule(Boolean type) {
		this.type = type;
	}
	
	public void demarrer() {
		this.rouler();
	}
	
	public abstract void rouler();

}
