package nonencapsule;

public class Client {
	
	public void crediterCompte(double credit) {
		CompteBancaire cb = new CompteBancaire();
		cb.crediter(100.12);
		cb.valeur = 200;
	}

}

class CompteBancaire {
	//public double valeur;
	private double valeur;
	public double crediter(double credit) {
		this.valeur+=credit;
		return this.valeur;
	}
	
	public double getValeur() {
		return this.valeur;
	}
	
	public void setValeur(double valeur) {
		this.valeur = valeur;
	}
}
